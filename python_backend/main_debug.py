import sys
import traceback
from engine.Engine import Engine

from engine.DataQueue import DataQueue
import threading
from time import sleep


class Main:
		
	def start_engine(self):
		try:
			if self.engine != None:
				self.engine.disconnect()
		except:
			traceback.print_exc()
	
	
		self.engine = Engine()
		
		job_thread = threading.Thread(target=self.engine.run)
		job_thread.start()
		print("job_thread.start()")
	
	
	def start_interface(self):
		while self.__monitor_messages:
			try:
				message = self.engine.get_interface_message()
				print("interface message: " + message[0] + " : " + message[1])
			except:
				#Logger.info("erro no start_interface")
				print("erro no start_interface")
				traceback.print_exc()
	


main = Main()
main.start_engine()



