import traceback

var MessageParser = new function() {

}


MessageParser.prototype.parseRPMMessage = function ( obdData ) {
	try:
		finalValue = (int(obdData[0][0], 16) * 256 + int(obdData[0][1], 16)) / 4
		print("RPM: " + str(finalValue) + " Tempo: " + str(obdData[1]))
	except:
		print('Erro ao efetuar parse de RPM: ', obdData[0])
		traceback.print_exc()
}

MessageParser.prototype.parseSpeetMessage = function ( obdData ) {
	try:
		value = obdData[0][0]
		finalValue = int(value, 16)
		print('speed: ' + str(finalValue) + ' Tempo: ' + str(obdData[1]))
	except:
		print('Erro ao efetuar parse de Speed: ', obdData[0])
		traceback.print_exc()

}


