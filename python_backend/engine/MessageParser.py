import traceback

class MessageParser:
	def __init__(self):
		print('construindo message parser'	)

	def parseRPMMessage(self, obdData):
		try:
			finalValue = (int(obdData[0][0], 16) * 256 + int(obdData[0][1], 16)) / 4
			print("RPM: " + str(finalValue) + " Tempo: " + str(obdData[1]))
		except:
			print('Erro ao efetuar parse de RPM: ', obdData[0])
			traceback.print_exc()
	
	def parseSpeedMessage(self, obdData):
		try:
			value = obdData[0][0]
			finalValue = int(value, 16)
			print('speed: ' + str(finalValue) + ' Tempo: ' + str(obdData[1]))
		except:
			print('Erro ao efetuar parse de Speed: ', obdData[0])
			traceback.print_exc()
