from communication.OBDSocket import OBDSocket
from engine.DataQueue import DataQueue
from engine.PIDSwitch import PIDSwitch
from engine.OBD2Mode01Pids2 import OBD2Mode01Pids2
import threading
import time
import traceback
from time import sleep
import schedule
from datetime import datetime, date
from Queue import *


var Engine = function() {
	this.remainingMessage = ""
	this.dataQueue = DataQueue()
	this.remainingMessage = ""
	this.obdSocket = None
	this.pidSwitch = PIDSwitch()
	this.obd2Mode01Pids2 = OBD2Mode01Pids2()
	this.sleep_time = 1
	
	this.consecutive_sent_msg = 0
	this.consecutive_sent_msg_limit = 10
	
	this.consecutive_errors = 0
	this.consecutive_errors_limit = 5
	
	this.sleep_time_calibrated = False
	this.calibration_tries = 0
	
	this.last_sent_message_time = datetime.now().time()
	this.last_received_message_time = datetime.now().time()
	this.lock = threading.Lock()
	
	this.reboot = False	
	this.soft_reboot = False
	this.run_main_thread = True
	
	this.thread_queue = Queue()
	
	this.send_message = True
}

Engine.prototype.connect = function() {
	print(" Connecting... ")
	self.obdSocket = OBDSocket('192.168.0.10', 35000)
	self.obdSocket.connect()
	self.obdSocket.deviceReset()
	self.obdSocket.setProtocol()
	self.obdSocket.removeEcho()
	print ('Engine is running...')
}

Engine.prototype.set_send_message = function( status ) {
	self.__lock.acquire()
	this.send_message = status
	self.__lock.release()
}

Engine.prototype.check_response_time_job = function() {
	while self.check_response_time:
		try:
			dt = ( datetime.combine(date.today(), self.__last_sent_message_time) - datetime.combine(date.today(), self.__last_received_message_time) ).total_seconds()
			print (" Time diference: " + str(dt))	
			if dt > 5:
				#print("$$$$$$$$$$$$$$$$$$$$  ERRO DE TIMEOUT ")
				self.increment_consecutive_errors_count()
			sleep(5)			
		except:
			traceback.print_exc()
}

Engine.prototype.increment_consecutive_sent_msg = function() {

	self.__lock.acquire()
	if self.__sleep_time_calibrated == False:
		self.__consecutive_sent_msg += 1
	
		if self.__consecutive_sent_msg > self.__consecutive_sent_msg_limit:
			self.__sleep_time_calibrated = True		
	self.__lock.release()

}

Engine.prototype.reset_consecutive_sent_msg = function() {
	self.__consecutive_sent_msg = 0
}

Engine.prototype.increment_consecutive_errors_count = function() {
	self.__lock.acquire()
	self.__consecutive_errors += 1
	self.reset_consecutive_sent_msg()
	#print("ERROS CONSECUTIVOS: " + str(self.__consecutive_errors))	
	if self.__consecutive_errors > self.__consecutive_errors_limit:
		self.reset_consecutive_errors_count()
		#self.calibrate_sleep_time()
		self.__soft_reboot = True
	self.__lock.release()
}		 
		
Engine.prototype.read_consecutive_errors_count = function() {
	self.__consecutive_errors = 0	
}

Engine.prototype.reset_consecutive_errors_count = function() {
	#print("RESETING CONSECUTIVE ERRORS COUNT")
}

Engine.prototype.get_interface_message = function() {
	return self.dataQueue.get_interface_message()
}

Engine.prototype.poll = function() {
	keys = self.pidSwitch.pids.keys()
	
	for key in keys:
		if self.pidSwitch.pids[key]:
			while ( self.send_message("01" + self.obd2Mode01Pids2.pids[key][0] + "1") == False ):
				#print("sleeping")
				sleep(0.01)
}	

Engine.prototype.poll_job = function() {
	while self.run_poll_thread:
		try:
			self.poll()
		except:
			traceback.print_exc()
			self.increment_consecutive_errors_count()		
}

Engine.prototype.send_diagnostic_trouble_codes_message = function() {
	send_message("03")	
}

Engine.prototype.send_message = function() {
	self.__last_sent_message_time = datetime.now().time()
	return self.send_message_to_device(message)	
}
	
Engine.prototype.send_message_to_device = function() {
	#self.__lock.acquire()
	if self.__send_message:
		if self.obdSocket != None:
			print("Sending message to device: " + message + " : " + str(datetime.now().time()))
			self.obdSocket.sendMessage(message)
			self.set_send_message(False)
		return True
	else:
		return False
	#self.__lock.release()
}

Engine.prototype.consume_messages_from_device = function() {
	try:
		reply = self.obdSocket.receive(4096)
	except:
		print("No data from socket")
		return
	
	msg = self.remainingMessage + reply

	if '>' in msg:
		self.set_send_message(True)

	self.remainingMessage = ""
	
	msgs = msg.split('\r')
				
	for i in range(len(msgs)):
		if ( i < len(msgs) ):
			expected_msg = self.dataQueue.send_message_to_response_queue(msgs[i])
			
			if expected_msg:	
				self.__last_received_message_time = datetime.now().time()
				self.reset_consecutive_errors_count()
		else:
			remainingMessage = msgs[i]

}

Engine.prototype.consume_messages_from_device_job = function() {
	while self.run_consume_messages_from_device_job:
		try:
			self.consume_messages_from_device()
			self.read_response_messages_mode_01_job()
			#sleep(0.5)	
		except:
			print("error while trying to get messages from device")
			traceback.print_exc()
			self.increment_consecutive_errors_count()
}
	
Engine.prototype.read_response_messages_mode_01_job = function() {
	try:
		msgs = self.dataQueue.read_response_messages_mode_01()
		
		for msg in msgs:
			self.dataQueue.add_interface_message(msg)	
	except:
		traceback.print_exc()
		self.increment_consecutive_errors_count()		

}
	
Engine.prototype.start_threads = function() {

	self.run_send_pending_messages_to_device_job = True
	self.run_consume_messages_from_device_job = True
	self.run_read_response_messages_mode_01_job = True
	self.run_poll_thread = True
	self.check_response_time = True
	
	consume_messages_from_device_job = threading.Thread(target=self.consume_messages_from_device_job)
	consume_messages_from_device_job.setName("consume_messages_from_device_job")
	consume_messages_from_device_job.start()
	
	poll_job_thread = threading.Thread(target=self.poll_job)
	poll_job_thread.setName("poll_job_thread")	
	poll_job_thread.start()	
	
	check_response_time_thread = threading.Thread(target=self.check_response_time_job)
	check_response_time_thread.setName("check_response_time_thread")	
	check_response_time_thread.start()	
	
	self.thread_queue.put(consume_messages_from_device_job)
	self.thread_queue.put(poll_job_thread)
	self.thread_queue.put(check_response_time_thread)	
	
	print("Threads iniciadas")		

}
	
Engine.prototype.stop_threads = function() {
	try:
		self.run_consume_messages_from_device_job = False
		self.run_poll_thread = False
		self.check_response_time = False

		while self.is_any_thread_alive():
			print("Waiting until all threads finish")
			sleep(1)
	except:
		print("error while trying to stop threads")
		traceback.print_exc()

}
	
Engine.prototype.disconnect = function() {
	try:
		print("disconnecting...")
		if self.obdSocket != None:
			self.obdSocket.close()
			self.obdSocket = None

	except:
		print("error while trying to stop threads and disconnecting from device")
		traceback.print_exc()		
}		
		
Engine.prototype.is_any_thread_alive = function() {
	while self.thread_queue.empty() == False:
		t = self.thread_queue.get()
		if t.isAlive():
			print t
			self.thread_queue.put(t)
			return True
		sleep(5)	

}

Engine.prototype.reboot = function() {
	self.__lock.acquire()
	self.stop_threads()
	self.disconnect()
	self.connect()
	self.start_threads()
	self.__lock.release()
	
	print("********************* Sleep time: " + str(self.sleep_time))
}
		
Engine.prototype.soft_reboot = function() {
	self.stop_threads()
	self.obdSocket.close()
	self.obdSocket.deviceReset()
	self.obdSocket.setProtocol()
	self.obdSocket.removeEcho()
	self.start_threads()
}

Engine.prototype.run = function() {
	self.reboot()				
	print ('Running engine')

	while self.__run_main_thread:
		sleep(5)

		try:
			if self.__soft_reboot:
				print("Rebooting...")
				self.__soft_reboot = False
				self.soft_reboot()

		except:
			traceback.print_exc()

}

