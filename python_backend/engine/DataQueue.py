#from multiprocessing import Queue
from Queue import *
import traceback
from datetime import datetime
from formulas.Formulas import FormulasMode01
from engine.MessageParser import MessageParser
from engine.OBD2Mode01Pids import OBD2Mode01Pids
from engine.OBD2Mode01Pids2 import OBD2Mode01Pids2
import threading

class DataQueue:

	__outbox_queue = Queue(maxsize=100)
	__response_queue_01 = Queue()
	__response_queue_03 = Queue()
	__interface_queue = Queue(10)
	
	mode01 = '41'	
	mode03 = '43'

	dict_pid_names  = OBD2Mode01Pids2().pids	
	dict_pid_values = OBD2Mode01Pids().pids		

	def add_interface_message(self, message):
		if self.__interface_queue.full() == False:	
			self.__interface_queue.put(message)	

	def get_interface_message(self):
		return self.__interface_queue.get()	
	

	def send_message(self, message):
		if self.__outbox_queue.full() == False:
			self.__outbox_queue.put(message, True)	

	def is_outbox_empty(self):
		return self.__outbox_queue.empty()

	def get_outbox_message(self):
		return self.__outbox_queue.get()		
	
						
	def send_message_to_response_queue(self, message):
		pids = message.split(" ")
		for i in range(0, len(pids)):
			pids[i] = pids[i].replace('\r','').replace('\n','')

		if pids[0] == self.mode01:
			obdData = (pids, datetime.now().time())
			self.__response_queue_01.put(obdData)
			return True

		if pids[0] == self.mode03:
			obdData = (pids, datetime.now().time())
			self.__response_queue_03.put(obdData)
			return True

		return False
	
	def read_response_messages_mode_01(self):
		l_msg = []
		while self.__response_queue_01.empty() == False:
			obdData = self.__response_queue_01.get()
			metric = self.dict_pid_values[obdData[0][1]][0]
			response_length = self.dict_pid_values[obdData[0][1]][1]
			
			calc_function = self.dict_pid_values[obdData[0][1]][2]
			
			x = obdData[0][2: 2 + response_length]

			value = calc_function(x)				

			msg = (metric, str(value))
			l_msg.append(msg)
		
		return l_msg
