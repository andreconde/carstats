# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/conde/apps/obdgpslogger/trunk/src/sim/bluetoothsimport.cc" "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/obdsim.dir/bluetoothsimport.cc.o"
  "/home/conde/apps/obdgpslogger/trunk/src/sim/fdsimport.cc" "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/obdsim.dir/fdsimport.cc.o"
  "/home/conde/apps/obdgpslogger/trunk/src/sim/mainloop.cc" "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/obdsim.dir/mainloop.cc.o"
  "/home/conde/apps/obdgpslogger/trunk/src/sim/obdsim.cc" "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/obdsim.dir/obdsim.cc.o"
  "/home/conde/apps/obdgpslogger/trunk/src/sim/posixsimport.cc" "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/obdsim.dir/posixsimport.cc.o"
  "/home/conde/apps/obdgpslogger/trunk/src/sim/simport.cc" "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/obdsim.dir/simport.cc.o"
  "/home/conde/apps/obdgpslogger/trunk/src/sim/socketsimport.cc" "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/obdsim.dir/socketsimport.cc.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "AVE_GETTIMEOFDAY"
  "HAVE_BLUETOOTH"
  "HAVE_POSIX_OPENPT"
  "HAVE_PTSNAME_R"
  "HAVE_SOCKET"
  "OBDGPSLOGGER_MAJOR_VERSION=0"
  "OBDGPSLOGGER_MINOR_VERSION=16"
  "OBDPLATFORM_POSIX"
  "OBDSIMGEN_CYCLE"
  "OBDSIMGEN_DLOPEN"
  "OBDSIMGEN_ERROR"
  "OBDSIMGEN_LOGGER"
  "OBDSIMGEN_RANDOM"
  "OBDSIMGEN_SOCKET"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/ckobdsim_error.dir/DependInfo.cmake"
  "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/ckobdsim_socket.dir/DependInfo.cmake"
  "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/ckobdsim_dlopen.dir/DependInfo.cmake"
  "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/ckobdsim_random.dir/DependInfo.cmake"
  "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/ckobdsim_cycle.dir/DependInfo.cmake"
  "/home/conde/apps/obdgpslogger/trunk/build/src/sim/CMakeFiles/ckobdsim_logger.dir/DependInfo.cmake"
  "/home/conde/apps/obdgpslogger/trunk/build/libs/sqlite3/CMakeFiles/cksqlite.dir/DependInfo.cmake"
  "/home/conde/apps/obdgpslogger/trunk/build/src/obdinfo/CMakeFiles/ckobdinfo.dir/DependInfo.cmake"
  "/home/conde/apps/obdgpslogger/trunk/build/src/conf/CMakeFiles/ckobdconfigfile.dir/DependInfo.cmake"
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "."
  "../src/obdinfo"
  "../src/conf"
  "../libs/sqlite3"
  "../src/sim/."
  "../src/sim/../obdinfo"
  "../src/sim/generators/dlopen"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
