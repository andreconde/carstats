#from socket import socket, gethostbyname, AF_INET, SOCK_DGRAM, SOCK_STREAM
import socket
import sys
import time
import traceback


class OBDSocket:

	def __init__(self, ip, port):
		self.ip = ip
		self.port = port

		try:
			#self.internalSocket = socket( AF_INET, SOCK_STREAM )
			self.internalSocket = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
		except:
			print('Failed while creating socket')
			raise
			
		
	def connect(self):
		try:
			self.internalSocket.connect((self.ip,self.port))
			self.internalSocket.settimeout(3)
		except:
			print('Failed while trying to connect to ip: ' + self.ip + ' and port: ' + str(self.port))
			traceback.print_exc()
			raise	

	def disconnect(self):
		try:
			self.internalSocket.close()
		except:
			print("Error while trying to disconnect")
			traceback.print_exc()
	
	
	def ensureIsConnected(self):
		count = 0

		while count < 200:
			self.sendMessage('01 00')		
			reply = self.receive()
			count = count + 1
			print('Trying to connect: ' + str(count))
			if '41 00' in reply:
				return True

		return False	
			

	def sendMessage(self, message):
		try:
			message = message + '\r'
			self.internalSocket.send(message)
		except:
			print('Erro ao enviar mensagem')
			traceback.print_exc()
	
	def deviceReset(self):
		self.sendMessage('AT Z')
	
	def removeEcho(self):
		self.sendMessage('AT E FF')
	
	def setProtocol(self):
		self.sendMessage('AT SP 3')

	def close(self):
		self.sendMessage('ATPC')
	
	def receive(self, length):
		reply = self.internalSocket.recv(length)
		#print("Receiving: " + reply.decode())
		return reply	
	
