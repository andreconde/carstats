#!/bin/sh
formulas_file="formulas/Formulas.py"
cat OBD4.csv | awk -F ';' '{print "\n\t@staticmethod\n\tdef " $4 "(*argv):\n\t\treturn 0"}' | grep -v "^\n"| sed 's/FormulasMode01.//' >> $formulas_file
