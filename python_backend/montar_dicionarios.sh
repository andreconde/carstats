#!/bin/sh

#@Author Andre Conde
# Shell script to create the dictionary based on the information provided by wikipedia
# https://en.wikipedia.org/wiki/OBD-II_PIDs

dict_1_class='OBD2Mode01Pids'
dict_1_file='engine/OBD2Mode01Pids.py'

echo "from formulas.Formulas import FormulasMode01\n" > $dict_1_file
echo "class $dict_1_class:\n" >> $dict_1_file
echo "\tpids = {" >> $dict_1_file

awk -F ";" '{print "\t\t""\""$1"\"" ":" "(""\""$2"\"" ", " $3 ", " $4"),"}' OBD4.csv | grep -v Metric >> $dict_1_file

echo "\t}" >> $dict_1_file


last_line=$(wc -l $dict_1_file | awk '{print $1}')
last_but_one_line=$(( $last_line - 1 ))
sed_line=$last_but_one_line"s"

sed "$sed_line/),/)/" $dict_1_file > /tmp/temp_dict
cat /tmp/temp_dict > $dict_1_file

#========================================================

dict_2_class='OBD2Mode01Pids2'
dict_2_file='engine/OBD2Mode01Pids2.py'

echo "from formulas.Formulas import FormulasMode01\n" > $dict_2_file
echo "class $dict_2_class:\n" >> $dict_2_file
echo "\tpids = {" >> $dict_2_file 

awk -F ";" '{print "\t\t""\""$2"\"" ":" "(""\""$1"\"" ", " $3 ", " $4"),"}' OBD4.csv | grep -v Metric >> $dict_2_file
echo "\t}" >> $dict_2_file


last_line=$(wc -l $dict_2_file | awk '{print $1}')
last_but_one_line=$(( $last_line - 1 ))
sed_line=$last_but_one_line"s"

sed "$sed_line/),/)/" $dict_2_file > /tmp/temp_dict
cat /tmp/temp_dict > $dict_2_file


#=========================================================
dict_3_class='PIDSwitch'
dict_3_file='engine/PIDSwitch.py'

echo "class $dict_3_class:\n" >> $dict_3_file
echo "\tpids = {" >> $dict_3_file

awk -F ";" '{print "\t\t""\""$2"\"" ":" "False,"}' OBD4.csv | grep -v Metric >> $dict_3_file


echo "\t}" >> $dict_3_file


last_line=$(wc -l $dict_3_file | awk '{print $1}')
last_but_one_line=$(( $last_line - 1 ))
sed_line=$last_but_one_line"s"

sed "$sed_line/),/)/" $dict_3_file > /tmp/temp_dict
cat /tmp/temp_dict > $dict_3_file




