import sys
import traceback
from engine.Engine import Engine


import kivy
#kivy.require('1.0.6') # replace with your current kivy version !
from kivy.logger import Logger
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.textinput import TextInput

from engine.DataQueue import DataQueue
import schedule
import threading
from time import sleep

class RootWidget(FloatLayout):

	def start_callback(self, *args):
		try:
			if self.engine != None:
				self.engine.disconnect()
		except:
			traceback.print_exc()
	

		self.engine = Engine()
		
		job_thread = threading.Thread(target=self.engine.run)
		job_thread.start()
		print("job_thread.start()")
		

	def __init__(self, **kwargs):
		# make sure we aren't overriding any important functionality
		super(RootWidget, self).__init__(**kwargs)
		self.__monitor_messages = True
		self.thread_start_interface = threading.Thread(target=self.start_interface)
		self.thread_start_interface.start()	
		self.start_button.bind(on_press=self.start_callback)
		#schedule.every(1).seconds.do(self.start_interface)
		


	def start_interface(self):
		while self.__monitor_messages:
			try:
				message = self.engine.get_interface_message()
				#print("interface message: " + message)
				self.label_msg.text = message[0] + "=" + message[1]
			except:
				#Logger.info("erro no start_interface")
				print("erro no start_interface")
				traceback.print_exc()
	



class MyApp(App):

	
	def start_callback(self, *args):
		try:
			if self.engine != None:
				self.engine.disconnect()
		except:
			traceback.print_exc()
	

		self.engine = Engine()
		
		job_thread = threading.Thread(target=self.engine.run)
		job_thread.start()
		print("job_thread.start()")


	def start_interface(self):
		while self.__monitor_messages:
			try:
				message = self.engine.get_interface_message()
				#print("interface message: " + message)
				if message[0] == 'engine_rpm':
					self.rpm_input.text = message[1]
					
				if message[0] == 'vehicle_speed':
					self.speed_input.text = message[1]

				if message[0] == 'engine_oil_temperature':
					self.engine_oil_temp_input.text = message[1]	

				if message[0] == 'engine_coolant_temperature':
					self.engine_coolant_temp_input.text = message[1]	 		 

				#self.label_msg.text = message[0] + "=" + message[1]
			except:
				#Logger.info("erro no start_interface")
				print("erro no start_interface")
				traceback.print_exc()

	def build(self):
		self.__monitor_messages = True
		
		
		main_layout = FloatLayout()
		box_vertical = BoxLayout(orientation='vertical')
		main_layout.add_widget(box_vertical)

		grid_1 = GridLayout(cols=4, padding=40)
		box_vertical.add_widget(grid_1)
	
		
		self.speed_input = TextInput(text='', size_hint_y=None, height=140)		
		self.rpm_input = TextInput(text='',size_hint_y=None, height=140)		
		self.engine_oil_temp_input = TextInput(text='', size_hint_y=None, height=40)		
		self.engine_coolant_temp_input = TextInput(text='', size_hint_y=None, height=40)		

		grid_1.add_widget(Label(text='Velocidade:', size_hint_y=None, height=40))
		grid_1.add_widget(self.speed_input)	

						
		grid_1.add_widget(Label(text='Rpm:', size_hint_y=None, height=40))
		grid_1.add_widget(self.rpm_input)	
					

		grid_1.add_widget(Label(text='Temperatura do oleo:', size_hint_y=None, height=40))
		grid_1.add_widget(self.engine_oil_temp_input)	

		grid_1.add_widget(Label(text='Temperatura do motor:', size_hint_y=None, height=40))
		grid_1.add_widget(self.engine_coolant_temp_input)	


		start_button = Button(text='Iniciar', size_hint_y=None, height=100)	
		start_button.bind(on_press=self.start_callback)

		box_vertical.add_widget(start_button)


		self.thread_start_interface = threading.Thread(target=self.start_interface)
		self.thread_start_interface.start()	

		return main_layout	

#	def run_engine(self):	
#		engine = Engine()
#		engine.run()

if __name__ == '__main__':
    MyApp().run()





