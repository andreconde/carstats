


function DataCollector() {

	var net = require('net');
	var Pids01 = require('./Pids01.js');
	this.util = require('util');
		
	var HOST = 'localhost';
	var PORT = 35000;

	this.client = new net.Socket();
	this.msgs_to_server = [];
	var msgs_to_client = [];
	this.msgs_to_client = msgs_to_client;	

	this.pids01 = new Pids01();	

	this.send = true;

	this.client.connect(PORT, HOST, function() {
	    console.log('CONNECTED TO: ' + HOST + ':' + PORT);
	    // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client 
	    //DataCollector.client.write('ATZ\r');
	});
	
	// Add a 'data' event handler for the client socket
	// data is what the server sent to this socket
	this.client.on('data', function(data) {
	    msgs_to_client.push( data );
	    console.log('DATA: ' + data);
		
	    // Close the client socket completely
	    
	});
	
	// Add a 'close' event handler for the client socket
	this.client.on('close', function() {
	    console.log('Connection closed');
	});

}


DataCollector.prototype.send_message = function(msg) {
	this.msgs_to_server.push( msg );
	//DataCollector.client.write(msg + '\r');
}

DataCollector.prototype.get_message = function() {
	return this.msgs_to_client.shift();
}

DataCollector.prototype.get_pending_message = function() {
	return this.msgs_to_server.shift();	
}

DataCollector.prototype.disconnect = function() {
	this.client.destroy();
}

DataCollector.prototype.read_messages = function() {
	var msg = this.get_message();
	if ( msg ) {
		console.log('received_msg: ' + msg);
		
			if ( msg.toString().indexOf(">") > -1 ) {
			this.send = true;
		}

	}	
}

DataCollector.prototype.send_pending_messages = function() {
	if ( this.send ) {
		var msg = this.get_pending_message();

		if ( msg ) {
			this.client.write( msg + '\r' );
			this.send = false;			
		}	
	}
}

DataCollector.prototype.poll_data = function() {
	var hash_2 = this.pids01.get_hash_2();
	var hash_3 = this.pids01.get_hash_3();
	
	//console.log( "hash_3: " + hash_3 );
	console.log(this.util.inspect(hash_2, false, null))
	var keys = Object.keys( hash_3 );

	for ( var i = 0; i < keys.length; i++ ) {
		if ( hash_3[keys[i]] ) {
			//console.log( "key: " + keys[i] );
			var msg = hash_2[keys[i]];
			this.send_message( "01" + msg[0] );
		}
	}

}

var dataCollector = new DataCollector()


setInterval( function(){ dataCollector.read_messages()}, 1000 ); 
setInterval( function(){dataCollector.poll_data()}, 1000 );
setInterval( function(){dataCollector.send_pending_messages()}, 1000 );

