
var Pids01 = module.exports = function() {
	this.hash_1 = {};
	this.hash_2 = {};
	this.hash_3 = {};

	this.hash_1["01"] = ["monitor_status_since_dtc_cleared", 4, Pids01.monitor_status_since_dtc_cleared];
	this.hash_1["02"] = ["freeze_dtc", 2, Pids01.freeze_dtc];
	this.hash_1["03"] = ["fuel_system_status", 2, Pids01.fuel_system_status];
	this.hash_1["04"] = ["calculated_engine_load", 1, Pids01.calculated_engine_load];
	this.hash_1["05"] = ["engine_coolant_temperature", 1, Pids01.engine_coolant_temperature];
	this.hash_1["06"] = ["short_term_fuel_trim_bank_1", 1, Pids01.short_term_fuel_trim_bank_1];
	this.hash_1["07"] = ["long_term_fuel_trim_bank_1", 1, Pids01.long_term_fuel_trim_bank_1];
	this.hash_1["08"] = ["short_term_fuel_trim_bank_2", 1, Pids01.short_term_fuel_trim_bank_2];
	this.hash_1["09"] = ["long_term_fuel_trim_bank_2", 1, Pids01.long_term_fuel_trim_bank_2];
	this.hash_1["0A"] = ["fuel_pressure", 1, Pids01.fuel_pressure];
	this.hash_1["0B"] = ["intake_manifold_absolute_pressure", 1, Pids01.intake_manifold_absolute_pressure];
	this.hash_1["0C"] = ["engine_rpm", 2, Pids01.engine_rpm];
	this.hash_1["0D"] = ["vehicle_speed", 1, Pids01.vehicle_speed];
	this.hash_1["0E"] = ["timing_advance", 1, Pids01.timing_advance];
	this.hash_1["0F"] = ["intake_air_temperature", 1, Pids01.intake_air_temperature];
	this.hash_1["10"] = ["maf_air_flow_rate", 2, Pids01.maf_air_flow_rate];
	this.hash_1["11"] = ["throttle_position", 1, Pids01.throttle_position];
	this.hash_1["12"] = ["commanded_secondary_air_status", 1, Pids01.commanded_secondary_air_status];
	this.hash_1["13"] = ["oxygen_sensor_present_in_2_banks", 1, Pids01.oxygen_sensor_present_in_2_banks];
	this.hash_1["14"] = ["oxygen_sensor_short_term_fuel_trim_1", 2, Pids01.oxygen_sensor_short_term_fuel_trim_1];
	this.hash_1["15"] = ["oxygen_sensor_short_term_fuel_trim_2", 2, Pids01.oxygen_sensor_short_term_fuel_trim_2];
	this.hash_1["16"] = ["oxygen_sensor_short_term_fuel_trim_3", 2, Pids01.oxygen_sensor_short_term_fuel_trim_3];
	this.hash_1["17"] = ["oxygen_sensor_short_term_fuel_trim_4", 2, Pids01.oxygen_sensor_short_term_fuel_trim_4];
	this.hash_1["18"] = ["oxygen_sensor_short_term_fuel_trim_5", 2, Pids01.oxygen_sensor_short_term_fuel_trim_5];
	this.hash_1["19"] = ["oxygen_sensor_short_term_fuel_trim_6", 2, Pids01.oxygen_sensor_short_term_fuel_trim_6];
	this.hash_1["1A"] = ["oxygen_sensor_short_term_fuel_trim_7", 2, Pids01.oxygen_sensor_short_term_fuel_trim_7];
	this.hash_1["1B"] = ["oxygen_sensor_short_term_fuel_trim_8", 2, Pids01.oxygen_sensor_short_term_fuel_trim_8];
	this.hash_1["1C"] = ["vehicle_conforms_to", 1, Pids01.vehicle_conforms_to];
	this.hash_1["1D"] = ["oxygen_sensor_present_in_4_banks", 1, Pids01.oxygen_sensor_present_in_4_banks];
	this.hash_1["1E"] = ["auxiliary_input_status", 1, Pids01.auxiliary_input_status];
	this.hash_1["1F"] = ["run_time_since_engine_start", 2, Pids01.run_time_since_engine_start];
	this.hash_1["20"] = ["pids_supported_21_40", 4, Pids01.pids_supported_21_40];
	this.hash_1["21"] = ["distance_traveled_with_mil", 2, Pids01.distance_traveled_with_mil];
	this.hash_1["22"] = ["fuel_rail_pressure", 2, Pids01.fuel_rail_pressure];
	this.hash_1["23"] = ["fuel_rail_gaugle_pressure", 2, Pids01.fuel_rail_gaugle_pressure];
	this.hash_1["24"] = ["oxygen_sensor_fuel_air_voltage_1", 4, Pids01.oxygen_sensor_fuel_air_voltage_1];
	this.hash_1["25"] = ["oxygen_sensor_fuel_air_voltage_2", 4, Pids01.oxygen_sensor_fuel_air_voltage_2];
	this.hash_1["26"] = ["oxygen_sensor_fuel_air_voltage_3", 4, Pids01.oxygen_sensor_fuel_air_voltage_3];
	this.hash_1["27"] = ["oxygen_sensor_fuel_air_voltage_4", 4, Pids01.oxygen_sensor_fuel_air_voltage_4];
	this.hash_1["28"] = ["oxygen_sensor_fuel_air_voltage_5", 4, Pids01.oxygen_sensor_fuel_air_voltage_5];
	this.hash_1["29"] = ["oxygen_sensor_fuel_air_voltage_6", 4, Pids01.oxygen_sensor_fuel_air_voltage_6];
	this.hash_1["2A"] = ["oxygen_sensor_fuel_air_voltage_7", 4, Pids01.oxygen_sensor_fuel_air_voltage_7];
	this.hash_1["2B"] = ["oxygen_sensor_fuel_air_voltage_8", 4, Pids01.oxygen_sensor_fuel_air_voltage_8];
	this.hash_1["2C"] = ["command_egr", 1, Pids01.command_egr];
	this.hash_1["2D"] = ["egr_error", 1, Pids01.egr_error];
	this.hash_1["2E"] = ["commanded_evaporative_purge", 1, Pids01.commanded_evaporative_purge];
	this.hash_1["2F"] = ["fuel_tank_level", 1, Pids01.fuel_tank_level];
	this.hash_1["30"] = ["warms_ups_since_codes_cleared", 1, Pids01.warms_ups_since_codes_cleared];
	this.hash_1["31"] = ["distance_traveled_since_codes_cleared", 2, Pids01.distance_traveled_since_codes_cleared];
	this.hash_1["32"] = ["evap_system_vapor_pressure", 2, Pids01.evap_system_vapor_pressure];
	this.hash_1["33"] = ["absolute_barometric_pressure", 1, Pids01.absolute_barometric_pressure];
	this.hash_1["34"] = ["oxygen_sensor_fuel_air_1_current", 4, Pids01.oxygen_sensor_fuel_air_1_current];
	this.hash_1["35"] = ["oxygen_sensor_fuel_air_2_current", 4, Pids01.oxygen_sensor_fuel_air_2_current];
	this.hash_1["36"] = ["oxygen_sensor_fuel_air_3_current", 4, Pids01.oxygen_sensor_fuel_air_3_current];
	this.hash_1["37"] = ["oxygen_sensor_fuel_air_4_current", 4, Pids01.oxygen_sensor_fuel_air_4_current];
	this.hash_1["38"] = ["oxygen_sensor_fuel_air_5_current", 4, Pids01.oxygen_sensor_fuel_air_5_current];
	this.hash_1["39"] = ["oxygen_sensor_fuel_air_6_current", 4, Pids01.oxygen_sensor_fuel_air_6_current];
	this.hash_1["3A"] = ["oxygen_sensor_fuel_air_7_current", 4, Pids01.oxygen_sensor_fuel_air_7_current];
	this.hash_1["3B"] = ["oxygen_sensor_fuel_air_8_current", 4, Pids01.oxygen_sensor_fuel_air_8_current];
	this.hash_1["3C"] = ["catalyst_temperature_bank1_sensor1", 2, Pids01.catalyst_temperature_bank1_sensor1];
	this.hash_1["3D"] = ["catalyst_temperature_bank2_sensor1", 2, Pids01.catalyst_temperature_bank2_sensor1];
	this.hash_1["3E"] = ["catalyst_temperature_bank1_sensor2", 2, Pids01.catalyst_temperature_bank1_sensor2];
	this.hash_1["3F"] = ["catalyst_temperature_bank2_sensor2", 2, Pids01.catalyst_temperature_bank2_sensor2];
	this.hash_1["40"] = ["pids_supported_41_60", 4, Pids01.pids_supported_41_60];
	this.hash_1["41"] = ["monitor_status_this_drive_cycle", 4, Pids01.monitor_status_this_drive_cycle];
	this.hash_1["42"] = ["control_module_voltage", 2, Pids01.control_module_voltage];
	this.hash_1["43"] = ["absolute_load_value", 2, Pids01.absolute_load_value];
	this.hash_1["44"] = ["fuel_air_commanded_equivalence_ratio", 2, Pids01.fuel_air_commanded_equivalence_ratio];
	this.hash_1["45"] = ["relative_throttle_position", 1, Pids01.relative_throttle_position];
	this.hash_1["46"] = ["ambiente_air_temperature", 1, Pids01.ambiente_air_temperature];
	this.hash_1["47"] = ["absolute_throttle_position_b", 1, Pids01.absolute_throttle_position_b];
	this.hash_1["48"] = ["absolute_throttle_position_c", 1, Pids01.absolute_throttle_position_c];
	this.hash_1["49"] = ["absolute_throttle_position_d", 1, Pids01.absolute_throttle_position_d];
	this.hash_1["4A"] = ["absolute_throttle_position_e", 1, Pids01.absolute_throttle_position_e];
	this.hash_1["4B"] = ["absolute_throttle_position_f", 1, Pids01.absolute_throttle_position_f];
	this.hash_1["4C"] = ["commanded_throttle_actuator", 1, Pids01.commanded_throttle_actuator];
	this.hash_1["4D"] = ["time_run_with_mil_on", 2, Pids01.time_run_with_mil_on];
	this.hash_1["4E"] = ["time_since_trouble_codes_cleared", 2, Pids01.time_since_trouble_codes_cleared];
	this.hash_1["4F"] = ["maximum_value_for_fuel_air_equivalence_ratio", 4, Pids01.maximum_value_for_fuel_air_equivalence_ratio];
	this.hash_1["50"] = ["maximum_value_for_air_flow_rate_from_mass_air_flow_sensor", 4, Pids01.maximum_value_for_air_flow_rate_from_mass_air_flow_sensor];
	this.hash_1["51"] = ["fuel_type", 1, Pids01.fuel_type];
	this.hash_1["52"] = ["ethanol_fuel", 1, Pids01.ethanol_fuel];
	this.hash_1["53"] = ["absolute_evap_system_vapor_pressure", 2, Pids01.absolute_evap_system_vapor_pressure];
	this.hash_1["54"] = ["evap_system_vapor_pressure_2", 2, Pids01.evap_system_vapor_pressure_2];
	this.hash_1["55"] = ["short_term_secondary_oxygen_sensor_trim_bank_1_3", 2, Pids01.short_term_secondary_oxygen_sensor_trim_bank_1_3];
	this.hash_1["56"] = ["long_term_secondary_oxygen_sensor_trim_bank_1_3", 2, Pids01.long_term_secondary_oxygen_sensor_trim_bank_1_3];
	this.hash_1["57"] = ["short_term_secondary_oxygen_sensor_trim_bank_2_4", 2, Pids01.short_term_secondary_oxygen_sensor_trim_bank_2_4];
	this.hash_1["58"] = ["long_term_secondary_oxygen_sensor_trim_bank_2_4", 2, Pids01.long_term_secondary_oxygen_sensor_trim_bank_2_4];
	this.hash_1["59"] = ["fuel_rail_absolute_pressure", 2, Pids01.fuel_rail_absolute_pressure];
	this.hash_1["5A"] = ["relative_accelerator_pedal_position", 1, Pids01.relative_accelerator_pedal_position];
	this.hash_1["5B"] = ["hybrid_battery_pack_remaining_life", 1, Pids01.hybrid_battery_pack_remaining_life];
	this.hash_1["5C"] = ["engine_oil_temperature", 1, Pids01.engine_oil_temperature];
	this.hash_1["5D"] = ["fuel_injection_timing", 2, Pids01.fuel_injection_timing];
	this.hash_1["5E"] = ["engine_fuel_rate", 2, Pids01.engine_fuel_rate];
	this.hash_1["5F"] = ["emission_requirements_vehicle_is_designed", 1, Pids01.emission_requirements_vehicle_is_designed];
	this.hash_1["60"] = ["pids_supported_61_80", 4, Pids01.pids_supported_61_80];
	this.hash_1["61"] = ["drivers_demand_engine_percent_torque", 1, Pids01.drivers_demand_engine_percent_torque];
	this.hash_1["62"] = ["actual_engine_percent_torque", 1, Pids01.actual_engine_percent_torque];
	this.hash_1["63"] = ["engine_reference_torque", 2, Pids01.engine_reference_torque];
	this.hash_1["64"] = ["engine_percent_torque_data", 5, Pids01.engine_percent_torque_data];
	this.hash_1["65"] = ["auxiliary_input_output_supported", 2, Pids01.auxiliary_input_output_supported];
	this.hash_1["66"] = ["mass_air_flow_sensor", 5, Pids01.mass_air_flow_sensor];
	this.hash_1["67"] = ["engine_coolant_temperature_2", 3, Pids01.engine_coolant_temperature_2];
	this.hash_1["68"] = ["intake_air_temperature_sensor", 7, Pids01.intake_air_temperature_sensor];
	this.hash_1["69"] = ["commanded_egr_and_egr_error", 7, Pids01.commanded_egr_and_egr_error];
	this.hash_1["6A"] = ["commanded_diesel_intake_air_flow_control_and_position", 5, Pids01.commanded_diesel_intake_air_flow_control_and_position];
	this.hash_1["6B"] = ["exhaust_gas_recirculation_temperature", 5, Pids01.exhaust_gas_recirculation_temperature];
	this.hash_1["6C"] = ["commanded_throttle_actuator_control_and_position", 5, Pids01.commanded_throttle_actuator_control_and_position];
	this.hash_1["6D"] = ["fuel_pressure_control_system", 6, Pids01.fuel_pressure_control_system];
	this.hash_1["6E"] = ["injection_pressure_control_system", 5, Pids01.injection_pressure_control_system];
	this.hash_1["6F"] = ["turbocharger_compressor_inlet_pressure", 3, Pids01.turbocharger_compressor_inlet_pressure];
	this.hash_1["70"] = ["boost_pressure_control", 9, Pids01.boost_pressure_control];
	this.hash_1["71"] = ["variable_geometry_turbo_vgt_control", 5, Pids01.variable_geometry_turbo_vgt_control];
	this.hash_1["72"] = ["wastegate_control", 5, Pids01.wastegate_control];
	this.hash_1["73"] = ["exhaust_pressure", 5, Pids01.exhaust_pressure];
	this.hash_1["74"] = ["turbo_charger_rpm", 5, Pids01.turbo_charger_rpm];
	this.hash_1["75"] = ["turbo_charger_temperature", 7, Pids01.turbo_charger_temperature];
	this.hash_1["76"] = ["turbo_charger_temperature_2", 7, Pids01.turbo_charger_temperature_2];
	this.hash_1["77"] = ["charge_air_cooler_temperature", 5, Pids01.charge_air_cooler_temperature];
	this.hash_1["78"] = ["exhaust_gas_temperature_bank_1", 9, Pids01.exhaust_gas_temperature_bank_1];
	this.hash_1["79"] = ["exhaust_gas_temperature_bank_2", 9, Pids01.exhaust_gas_temperature_bank_2];
	this.hash_1["7A"] = ["diesel_particulate_filter_1", 7, Pids01.diesel_particulate_filter_1];
	this.hash_1["7B"] = ["diesel_particulate_filter_2", 7, Pids01.diesel_particulate_filter_2];
	this.hash_1["7C"] = ["diesel_particulate_filter_3", 9, Pids01.diesel_particulate_filter_3];
	this.hash_1["7D"] = ["nox_nte_control_area_status", 1, Pids01.nox_nte_control_area_status];
	this.hash_1["7E"] = ["pm_nte_control_area_status", 1, Pids01.pm_nte_control_area_status];
	this.hash_1["7F"] = ["engine_run_time", 13, Pids01.engine_run_time];
	this.hash_1["80"] = ["pids_supported_81_A0", 4, Pids01.pids_supported_81_A0];
	this.hash_1["81"] = ["engine_run_time_aecd_1", 21, Pids01.engine_run_time_aecd_1];
	this.hash_1["82"] = ["engine_run_time_aecd_2", 21, Pids01.engine_run_time_aecd_2];
	this.hash_1["83"] = ["nox_sensor", 5, Pids01.nox_sensor];
	this.hash_1["84"] = ["manifold_surface_temperature", 0, Pids01.manifold_surface_temperature];
	this.hash_1["85"] = ["nox_reagend_system", 0, Pids01.nox_reagend_system];
	this.hash_1["86"] = ["pm_sensor", 0, Pids01.pm_sensor];
	this.hash_1["87"] = ["intake_manifold_absolute_pressure_2", 0, Pids01.intake_manifold_absolute_pressure_2];
	this.hash_1["A0"] = ["pids_supported_A1_C9", 4, Pids01.pids_supported_A1_C9];
	this.hash_1["C0"] = ["pids_supported_C1_E0", 4, Pids01.pids_supported_C1_E0];
	this.hash_1["C3"] = ["	return_numerous_data", 0, Pids01.return_numerous_data]; 
	this.hash_1["C4"] = ["engine_idle_request", 0, Pids01.engine_idle_request];

	
	this.hash_2["monitor_status_since_dtc_cleared"] = ["01", 4, Pids01.monitor_status_since_dtc_cleared];
	this.hash_2["freeze_dtc"] = ["02", 2, Pids01.freeze_dtc];
	this.hash_2["fuel_system_status"] = ["03", 2, Pids01.fuel_system_status];
	this.hash_2["calculated_engine_load"] = ["04", 1, Pids01.calculated_engine_load];
	this.hash_2["engine_coolant_temperature"] = ["05", 1, Pids01.engine_coolant_temperature];
	this.hash_2["short_term_fuel_trim_bank_1"] = ["06", 1, Pids01.short_term_fuel_trim_bank_1];
	this.hash_2["long_term_fuel_trim_bank_1"] = ["07", 1, Pids01.long_term_fuel_trim_bank_1];
	this.hash_2["short_term_fuel_trim_bank_2"] = ["08", 1, Pids01.short_term_fuel_trim_bank_2];
	this.hash_2["long_term_fuel_trim_bank_2"] = ["09", 1, Pids01.long_term_fuel_trim_bank_2];
	this.hash_2["fuel_pressure"] = ["0A", 1, Pids01.fuel_pressure];
	this.hash_2["intake_manifold_absolute_pressure"] = ["0B", 1, Pids01.intake_manifold_absolute_pressure];
	this.hash_2["engine_rpm"] = ["0C", 2, Pids01.engine_rpm];
	this.hash_2["vehicle_speed"] = ["0D", 1, Pids01.vehicle_speed];
	this.hash_2["timing_advance"] = ["0E", 1, Pids01.timing_advance];
	this.hash_2["intake_air_temperature"] = ["0F", 1, Pids01.intake_air_temperature];
	this.hash_2["maf_air_flow_rate"] = ["10", 2, Pids01.maf_air_flow_rate];
	this.hash_2["throttle_position"] = ["11", 1, Pids01.throttle_position];
	this.hash_2["commanded_secondary_air_status"] = ["12", 1, Pids01.commanded_secondary_air_status];
	this.hash_2["oxygen_sensor_present_in_2_banks"] = ["13", 1, Pids01.oxygen_sensor_present_in_2_banks];
	this.hash_2["oxygen_sensor_short_term_fuel_trim_1"] = ["14", 2, Pids01.oxygen_sensor_short_term_fuel_trim_1];
	this.hash_2["oxygen_sensor_short_term_fuel_trim_2"] = ["15", 2, Pids01.oxygen_sensor_short_term_fuel_trim_2];
	this.hash_2["oxygen_sensor_short_term_fuel_trim_3"] = ["16", 2, Pids01.oxygen_sensor_short_term_fuel_trim_3];
	this.hash_2["oxygen_sensor_short_term_fuel_trim_4"] = ["17", 2, Pids01.oxygen_sensor_short_term_fuel_trim_4];
	this.hash_2["oxygen_sensor_short_term_fuel_trim_5"] = ["18", 2, Pids01.oxygen_sensor_short_term_fuel_trim_5];
	this.hash_2["oxygen_sensor_short_term_fuel_trim_6"] = ["19", 2, Pids01.oxygen_sensor_short_term_fuel_trim_6];
	this.hash_2["oxygen_sensor_short_term_fuel_trim_7"] = ["1A", 2, Pids01.oxygen_sensor_short_term_fuel_trim_7];
	this.hash_2["oxygen_sensor_short_term_fuel_trim_8"] = ["1B", 2, Pids01.oxygen_sensor_short_term_fuel_trim_8];
	this.hash_2["vehicle_conforms_to"] = ["1C", 1, Pids01.vehicle_conforms_to];
	this.hash_2["oxygen_sensor_present_in_4_banks"] = ["1D", 1, Pids01.oxygen_sensor_present_in_4_banks];
	this.hash_2["auxiliary_input_status"] = ["1E", 1, Pids01.auxiliary_input_status];
	this.hash_2["run_time_since_engine_start"] = ["1F", 2, Pids01.run_time_since_engine_start];
	this.hash_2["pids_supported_21_40"] = ["20", 4, Pids01.pids_supported_21_40];
	this.hash_2["distance_traveled_with_mil"] = ["21", 2, Pids01.distance_traveled_with_mil];
	this.hash_2["fuel_rail_pressure"] = ["22", 2, Pids01.fuel_rail_pressure];
	this.hash_2["fuel_rail_gaugle_pressure"] = ["23", 2, Pids01.fuel_rail_gaugle_pressure];
	this.hash_2["oxygen_sensor_fuel_air_voltage_1"] = ["24", 4, Pids01.oxygen_sensor_fuel_air_voltage_1];
	this.hash_2["oxygen_sensor_fuel_air_voltage_2"] = ["25", 4, Pids01.oxygen_sensor_fuel_air_voltage_2];
	this.hash_2["oxygen_sensor_fuel_air_voltage_3"] = ["26", 4, Pids01.oxygen_sensor_fuel_air_voltage_3];
	this.hash_2["oxygen_sensor_fuel_air_voltage_4"] = ["27", 4, Pids01.oxygen_sensor_fuel_air_voltage_4];
	this.hash_2["oxygen_sensor_fuel_air_voltage_5"] = ["28", 4, Pids01.oxygen_sensor_fuel_air_voltage_5];
	this.hash_2["oxygen_sensor_fuel_air_voltage_6"] = ["29", 4, Pids01.oxygen_sensor_fuel_air_voltage_6];
	this.hash_2["oxygen_sensor_fuel_air_voltage_7"] = ["2A", 4, Pids01.oxygen_sensor_fuel_air_voltage_7];
	this.hash_2["oxygen_sensor_fuel_air_voltage_8"] = ["2B", 4, Pids01.oxygen_sensor_fuel_air_voltage_8];
	this.hash_2["command_egr"] = ["2C", 1, Pids01.command_egr];
	this.hash_2["egr_error"] = ["2D", 1, Pids01.egr_error];
	this.hash_2["commanded_evaporative_purge"] = ["2E", 1, Pids01.commanded_evaporative_purge];
	this.hash_2["fuel_tank_level"] = ["2F", 1, Pids01.fuel_tank_level];
	this.hash_2["warms_ups_since_codes_cleared"] = ["30", 1, Pids01.warms_ups_since_codes_cleared];
	this.hash_2["distance_traveled_since_codes_cleared"] = ["31", 2, Pids01.distance_traveled_since_codes_cleared];
	this.hash_2["evap_system_vapor_pressure"] = ["32", 2, Pids01.evap_system_vapor_pressure];
	this.hash_2["absolute_barometric_pressure"] = ["33", 1, Pids01.absolute_barometric_pressure];
	this.hash_2["oxygen_sensor_fuel_air_1_current"] = ["34", 4, Pids01.oxygen_sensor_fuel_air_1_current];
	this.hash_2["oxygen_sensor_fuel_air_2_current"] = ["35", 4, Pids01.oxygen_sensor_fuel_air_2_current];
	this.hash_2["oxygen_sensor_fuel_air_3_current"] = ["36", 4, Pids01.oxygen_sensor_fuel_air_3_current];
	this.hash_2["oxygen_sensor_fuel_air_4_current"] = ["37", 4, Pids01.oxygen_sensor_fuel_air_4_current];
	this.hash_2["oxygen_sensor_fuel_air_5_current"] = ["38", 4, Pids01.oxygen_sensor_fuel_air_5_current];
	this.hash_2["oxygen_sensor_fuel_air_6_current"] = ["39", 4, Pids01.oxygen_sensor_fuel_air_6_current];
	this.hash_2["oxygen_sensor_fuel_air_7_current"] = ["3A", 4, Pids01.oxygen_sensor_fuel_air_7_current];
	this.hash_2["oxygen_sensor_fuel_air_8_current"] = ["3B", 4, Pids01.oxygen_sensor_fuel_air_8_current];
	this.hash_2["catalyst_temperature_bank1_sensor1"] = ["3C", 2, Pids01.catalyst_temperature_bank1_sensor1];
	this.hash_2["catalyst_temperature_bank2_sensor1"] = ["3D", 2, Pids01.catalyst_temperature_bank2_sensor1];
	this.hash_2["catalyst_temperature_bank1_sensor2"] = ["3E", 2, Pids01.catalyst_temperature_bank1_sensor2];
	this.hash_2["catalyst_temperature_bank2_sensor2"] = ["3F", 2, Pids01.catalyst_temperature_bank2_sensor2];
	this.hash_2["pids_supported_41_60"] = ["40", 4, Pids01.pids_supported_41_60];
	this.hash_2["monitor_status_this_drive_cycle"] = ["41", 4, Pids01.monitor_status_this_drive_cycle];
	this.hash_2["control_module_voltage"] = ["42", 2, Pids01.control_module_voltage];
	this.hash_2["absolute_load_value"] = ["43", 2, Pids01.absolute_load_value];
	this.hash_2["fuel_air_commanded_equivalence_ratio"] = ["44", 2, Pids01.fuel_air_commanded_equivalence_ratio];
	this.hash_2["relative_throttle_position"] = ["45", 1, Pids01.relative_throttle_position];
	this.hash_2["ambiente_air_temperature"] = ["46", 1, Pids01.ambiente_air_temperature];
	this.hash_2["absolute_throttle_position_b"] = ["47", 1, Pids01.absolute_throttle_position_b];
	this.hash_2["absolute_throttle_position_c"] = ["48", 1, Pids01.absolute_throttle_position_c];
	this.hash_2["absolute_throttle_position_d"] = ["49", 1, Pids01.absolute_throttle_position_d];
	this.hash_2["absolute_throttle_position_e"] = ["4A", 1, Pids01.absolute_throttle_position_e];
	this.hash_2["absolute_throttle_position_f"] = ["4B", 1, Pids01.absolute_throttle_position_f];
	this.hash_2["commanded_throttle_actuator"] = ["4C", 1, Pids01.commanded_throttle_actuator];
	this.hash_2["time_run_with_mil_on"] = ["4D", 2, Pids01.time_run_with_mil_on];
	this.hash_2["time_since_trouble_codes_cleared"] = ["4E", 2, Pids01.time_since_trouble_codes_cleared];
	this.hash_2["maximum_value_for_fuel_air_equivalence_ratio"] = ["4F", 4, Pids01.maximum_value_for_fuel_air_equivalence_ratio];
	this.hash_2["maximum_value_for_air_flow_rate_from_mass_air_flow_sensor"] = ["50", 4, Pids01.maximum_value_for_air_flow_rate_from_mass_air_flow_sensor];
	this.hash_2["fuel_type"] = ["51", 1, Pids01.fuel_type];
	this.hash_2["ethanol_fuel"] = ["52", 1, Pids01.ethanol_fuel];
	this.hash_2["absolute_evap_system_vapor_pressure"] = ["53", 2, Pids01.absolute_evap_system_vapor_pressure];
	this.hash_2["evap_system_vapor_pressure_2"] = ["54", 2, Pids01.evap_system_vapor_pressure_2];
	this.hash_2["short_term_secondary_oxygen_sensor_trim_bank_1_3"] = ["55", 2, Pids01.short_term_secondary_oxygen_sensor_trim_bank_1_3];
	this.hash_2["long_term_secondary_oxygen_sensor_trim_bank_1_3"] = ["56", 2, Pids01.long_term_secondary_oxygen_sensor_trim_bank_1_3];
	this.hash_2["short_term_secondary_oxygen_sensor_trim_bank_2_4"] = ["57", 2, Pids01.short_term_secondary_oxygen_sensor_trim_bank_2_4];
	this.hash_2["long_term_secondary_oxygen_sensor_trim_bank_2_4"] = ["58", 2, Pids01.long_term_secondary_oxygen_sensor_trim_bank_2_4];
	this.hash_2["fuel_rail_absolute_pressure"] = ["59", 2, Pids01.fuel_rail_absolute_pressure];
	this.hash_2["relative_accelerator_pedal_position"] = ["5A", 1, Pids01.relative_accelerator_pedal_position];
	this.hash_2["hybrid_battery_pack_remaining_life"] = ["5B", 1, Pids01.hybrid_battery_pack_remaining_life];
	this.hash_2["engine_oil_temperature"] = ["5C", 1, Pids01.engine_oil_temperature];
	this.hash_2["fuel_injection_timing"] = ["5D", 2, Pids01.fuel_injection_timing];
	this.hash_2["engine_fuel_rate"] = ["5E", 2, Pids01.engine_fuel_rate];
	this.hash_2["emission_requirements_vehicle_is_designed"] = ["5F", 1, Pids01.emission_requirements_vehicle_is_designed];
	this.hash_2["pids_supported_61_80"] = ["60", 4, Pids01.pids_supported_61_80];
	this.hash_2["drivers_demand_engine_percent_torque"] = ["61", 1, Pids01.drivers_demand_engine_percent_torque];
	this.hash_2["actual_engine_percent_torque"] = ["62", 1, Pids01.actual_engine_percent_torque];
	this.hash_2["engine_reference_torque"] = ["63", 2, Pids01.engine_reference_torque];
	this.hash_2["engine_percent_torque_data"] = ["64", 5, Pids01.engine_percent_torque_data];
	this.hash_2["auxiliary_input_output_supported"] = ["65", 2, Pids01.auxiliary_input_output_supported];
	this.hash_2["mass_air_flow_sensor"] = ["66", 5, Pids01.mass_air_flow_sensor];
	this.hash_2["engine_coolant_temperature_2"] = ["67", 3, Pids01.engine_coolant_temperature_2];
	this.hash_2["intake_air_temperature_sensor"] = ["68", 7, Pids01.intake_air_temperature_sensor];
	this.hash_2["commanded_egr_and_egr_error"] = ["69", 7, Pids01.commanded_egr_and_egr_error];
	this.hash_2["commanded_diesel_intake_air_flow_control_and_position"] = ["6A", 5, Pids01.commanded_diesel_intake_air_flow_control_and_position];
	this.hash_2["exhaust_gas_recirculation_temperature"] = ["6B", 5, Pids01.exhaust_gas_recirculation_temperature];
	this.hash_2["commanded_throttle_actuator_control_and_position"] = ["6C", 5, Pids01.commanded_throttle_actuator_control_and_position];
	this.hash_2["fuel_pressure_control_system"] = ["6D", 6, Pids01.fuel_pressure_control_system];
	this.hash_2["injection_pressure_control_system"] = ["6E", 5, Pids01.injection_pressure_control_system];
	this.hash_2["turbocharger_compressor_inlet_pressure"] = ["6F", 3, Pids01.turbocharger_compressor_inlet_pressure];
	this.hash_2["boost_pressure_control"] = ["70", 9, Pids01.boost_pressure_control];
	this.hash_2["variable_geometry_turbo_vgt_control"] = ["71", 5, Pids01.variable_geometry_turbo_vgt_control];
	this.hash_2["wastegate_control"] = ["72", 5, Pids01.wastegate_control];
	this.hash_2["exhaust_pressure"] = ["73", 5, Pids01.exhaust_pressure];
	this.hash_2["turbo_charger_rpm"] = ["74", 5, Pids01.turbo_charger_rpm];
	this.hash_2["turbo_charger_temperature"] = ["75", 7, Pids01.turbo_charger_temperature];
	this.hash_2["turbo_charger_temperature_2"] = ["76", 7, Pids01.turbo_charger_temperature_2];
	this.hash_2["charge_air_cooler_temperature"] = ["77", 5, Pids01.charge_air_cooler_temperature];
	this.hash_2["exhaust_gas_temperature_bank_1"] = ["78", 9, Pids01.exhaust_gas_temperature_bank_1];
	this.hash_2["exhaust_gas_temperature_bank_2"] = ["79", 9, Pids01.exhaust_gas_temperature_bank_2];
	this.hash_2["diesel_particulate_filter_1"] = ["7A", 7, Pids01.diesel_particulate_filter_1];
	this.hash_2["diesel_particulate_filter_2"] = ["7B", 7, Pids01.diesel_particulate_filter_2];
	this.hash_2["diesel_particulate_filter_3"] = ["7C", 9, Pids01.diesel_particulate_filter_3];
	this.hash_2["nox_nte_control_area_status"] = ["7D", 1, Pids01.nox_nte_control_area_status];
	this.hash_2["pm_nte_control_area_status"] = ["7E", 1, Pids01.pm_nte_control_area_status];
	this.hash_2["engine_run_time"] = ["7F", 13, Pids01.engine_run_time];
	this.hash_2["pids_supported_81_A0"] = ["80", 4, Pids01.pids_supported_81_A0];
	this.hash_2["engine_run_time_aecd_1"] = ["81", 21, Pids01.engine_run_time_aecd_1];
	this.hash_2["engine_run_time_aecd_2"] = ["82", 21, Pids01.engine_run_time_aecd_2];
	this.hash_2["nox_sensor"] = ["83", 5, Pids01.nox_sensor];
	this.hash_2["manifold_surface_temperature"] = ["84", 0, Pids01.manifold_surface_temperature];
	this.hash_2["nox_reagend_system"] = ["85", 0, Pids01.nox_reagend_system];
	this.hash_2["pm_sensor"] = ["86", 0, Pids01.pm_sensor];
	this.hash_2["intake_manifold_absolute_pressure_2"] = ["87", 0, Pids01.intake_manifold_absolute_pressure_2];
	this.hash_2["pids_supported_A1_C9"] = ["A0", 4, Pids01.pids_supported_A1_C9];
	this.hash_2["pids_supported_C1_E0"] = ["C0", 4, Pids01.pids_supported_C1_E0];
	this.hash_2["	return_numerous_data"] = ["C3", 0, Pids01.return_numerous_data]; 
	this.hash_2["engine_idle_request"] = ["C4", 0, Pids01.engine_idle_request];	


	this.hash_3["monitor_status_since_dtc_cleared"]  = false;
	this.hash_3["freeze_dtc"]  = false;
	this.hash_3["fuel_system_status"]  = false;
	this.hash_3["calculated_engine_load"]  = false;
	this.hash_3["engine_coolant_temperature"]  = false;
	this.hash_3["short_term_fuel_trim_bank_1"]  = false;
	this.hash_3["long_term_fuel_trim_bank_1"]  = false;
	this.hash_3["short_term_fuel_trim_bank_2"]  = false;
	this.hash_3["long_term_fuel_trim_bank_2"]  = false;
	this.hash_3["fuel_pressure"]  = false;
	this.hash_3["intake_manifold_absolute_pressure"]  = false;
	this.hash_3["engine_rpm"]  = true;
	this.hash_3["vehicle_speed"]  = false;
	this.hash_3["timing_advance"]  = false;
	this.hash_3["intake_air_temperature"]  = false;
	this.hash_3["maf_air_flow_rate"]  = false;
	this.hash_3["throttle_position"]  = false;
	this.hash_3["commanded_secondary_air_status"]  = false;
	this.hash_3["oxygen_sensor_present_in_2_banks"]  = false;
	this.hash_3["oxygen_sensor_short_term_fuel_trim_1"]  = false;
	this.hash_3["oxygen_sensor_short_term_fuel_trim_2"]  = false;
	this.hash_3["oxygen_sensor_short_term_fuel_trim_3"]  = false;
	this.hash_3["oxygen_sensor_short_term_fuel_trim_4"]  = false;
	this.hash_3["oxygen_sensor_short_term_fuel_trim_5"]  = false;
	this.hash_3["oxygen_sensor_short_term_fuel_trim_6"]  = false;
	this.hash_3["oxygen_sensor_short_term_fuel_trim_7"]  = false;
	this.hash_3["oxygen_sensor_short_term_fuel_trim_8"]  = false;
	this.hash_3["vehicle_conforms_to"]  = false;
	this.hash_3["oxygen_sensor_present_in_4_banks"]  = false;
	this.hash_3["auxiliary_input_status"]  = false;
	this.hash_3["run_time_since_engine_start"]  = false;
	this.hash_3["pids_supported_21_40"]  = false;
	this.hash_3["distance_traveled_with_mil"]  = false;
	this.hash_3["fuel_rail_pressure"]  = false;
	this.hash_3["fuel_rail_gaugle_pressure"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_voltage_1"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_voltage_2"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_voltage_3"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_voltage_4"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_voltage_5"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_voltage_6"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_voltage_7"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_voltage_8"]  = false;
	this.hash_3["command_egr"]  = false;
	this.hash_3["egr_error"]  = false;
	this.hash_3["commanded_evaporative_purge"]  = false;
	this.hash_3["fuel_tank_level"]  = false;
	this.hash_3["warms_ups_since_codes_cleared"]  = false;
	this.hash_3["distance_traveled_since_codes_cleared"]  = false;
	this.hash_3["evap_system_vapor_pressure"]  = false;
	this.hash_3["absolute_barometric_pressure"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_1_current"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_2_current"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_3_current"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_4_current"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_5_current"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_6_current"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_7_current"]  = false;
	this.hash_3["oxygen_sensor_fuel_air_8_current"]  = false;
	this.hash_3["catalyst_temperature_bank1_sensor1"]  = false;
	this.hash_3["catalyst_temperature_bank2_sensor1"]  = false;
	this.hash_3["catalyst_temperature_bank1_sensor2"]  = false;
	this.hash_3["catalyst_temperature_bank2_sensor2"]  = false;
	this.hash_3["pids_supported_41_60"]  = false;
	this.hash_3["monitor_status_this_drive_cycle"]  = false;
	this.hash_3["control_module_voltage"]  = false;
	this.hash_3["absolute_load_value"]  = false;
	this.hash_3["fuel_air_commanded_equivalence_ratio"]  = false;
	this.hash_3["relative_throttle_position"]  = false;
	this.hash_3["ambiente_air_temperature"]  = false;
	this.hash_3["absolute_throttle_position_b"]  = false;
	this.hash_3["absolute_throttle_position_c"]  = false;
	this.hash_3["absolute_throttle_position_d"]  = false;
	this.hash_3["absolute_throttle_position_e"]  = false;
	this.hash_3["absolute_throttle_position_f"]  = false;
	this.hash_3["commanded_throttle_actuator"]  = false;
	this.hash_3["time_run_with_mil_on"]  = false;
	this.hash_3["time_since_trouble_codes_cleared"]  = false;
	this.hash_3["maximum_value_for_fuel_air_equivalence_ratio"]  = false;
	this.hash_3["maximum_value_for_air_flow_rate_from_mass_air_flow_sensor"]  = false;
	this.hash_3["fuel_type"]  = false;
	this.hash_3["ethanol_fuel"]  = false;
	this.hash_3["absolute_evap_system_vapor_pressure"]  = false;
	this.hash_3["evap_system_vapor_pressure_2"]  = false;
	this.hash_3["short_term_secondary_oxygen_sensor_trim_bank_1_3"]  = false;
	this.hash_3["long_term_secondary_oxygen_sensor_trim_bank_1_3"]  = false;
	this.hash_3["short_term_secondary_oxygen_sensor_trim_bank_2_4"]  = false;
	this.hash_3["long_term_secondary_oxygen_sensor_trim_bank_2_4"]  = false;
	this.hash_3["fuel_rail_absolute_pressure"]  = false;
	this.hash_3["relative_accelerator_pedal_position"]  = false;
	this.hash_3["hybrid_battery_pack_remaining_life"]  = false;
	this.hash_3["engine_oil_temperature"]  = false;
	this.hash_3["fuel_injection_timing"]  = false;
	this.hash_3["engine_fuel_rate"]  = false;
	this.hash_3["emission_requirements_vehicle_is_designed"]  = false;
	this.hash_3["pids_supported_61_80"]  = false;
	this.hash_3["drivers_demand_engine_percent_torque"]  = false;
	this.hash_3["actual_engine_percent_torque"]  = false;
	this.hash_3["engine_reference_torque"]  = false;
	this.hash_3["engine_percent_torque_data"]  = false;
	this.hash_3["auxiliary_input_output_supported"]  = false;
	this.hash_3["mass_air_flow_sensor"]  = false;
	this.hash_3["engine_coolant_temperature_2"]  = false;
	this.hash_3["intake_air_temperature_sensor"]  = false;
	this.hash_3["commanded_egr_and_egr_error"]  = false;
	this.hash_3["commanded_diesel_intake_air_flow_control_and_position"]  = false;
	this.hash_3["exhaust_gas_recirculation_temperature"]  = false;
	this.hash_3["commanded_throttle_actuator_control_and_position"]  = false;
	this.hash_3["fuel_pressure_control_system"]  = false;
	this.hash_3["injection_pressure_control_system"]  = false;
	this.hash_3["turbocharger_compressor_inlet_pressure"]  = false;
	this.hash_3["boost_pressure_control"]  = false;
	this.hash_3["variable_geometry_turbo_vgt_control"]  = false;
	this.hash_3["wastegate_control"]  = false;
	this.hash_3["exhaust_pressure"]  = false;
	this.hash_3["turbo_charger_rpm"]  = false;
	this.hash_3["turbo_charger_temperature"]  = false;
	this.hash_3["turbo_charger_temperature_2"]  = false;
	this.hash_3["charge_air_cooler_temperature"]  = false;
	this.hash_3["exhaust_gas_temperature_bank_1"]  = false;
	this.hash_3["exhaust_gas_temperature_bank_2"]  = false;
	this.hash_3["diesel_particulate_filter_1"]  = false;
	this.hash_3["diesel_particulate_filter_2"]  = false;
	this.hash_3["diesel_particulate_filter_3"]  = false;
	this.hash_3["nox_nte_control_area_status"]  = false;
	this.hash_3["pm_nte_control_area_status"]  = false;
	this.hash_3["engine_run_time"]  = false;
	this.hash_3["pids_supported_81_A0"]  = false;
	this.hash_3["engine_run_time_aecd_1"]  = false;
	this.hash_3["engine_run_time_aecd_2"]  = false;
	this.hash_3["nox_sensor"]  = false;
	this.hash_3["manifold_surface_temperature"]  = false;
	this.hash_3["nox_reagend_system"]  = false;
	this.hash_3["pm_sensor"]  = false;
	this.hash_3["intake_manifold_absolute_pressure_2"]  = false;
	this.hash_3["pids_supported_A1_C9"]  = false;
	this.hash_3["pids_supported_C1_E0"]  = false;
	this.hash_3["return_numerous_data"]  = false;
	this.hash_3["engine_idle_request"]  = false;

}

	Pids01.prototype.get_hash_1 = function() {
		return this.hash_1;
	}

	
	Pids01.prototype.get_hash_2 = function() {
		return this.hash_2;
	}

	
	Pids01.prototype.get_hash_3 = function() {
		return this.hash_3;
	}

	Pids01.engine_load = function(args) {
			return int("0x" + "".join(args[0]), 16)/255  
	}
      
        Pids01.engine_coolant_temperature = function(args) {
        		return int("0x" + "".join(args[0]), 16) - 40 
  	}
  
        Pids01.engine_rpm = function(args) {
        	//return ( int("0x" + "".join(args[0]), 16) * 256 + int("0x" + "".join(args[1]),16) ) / 4 
		return 0;	
  	}
  
        Pids01.vehicle_speed = function(args) {
        	return int("0x" + "".join(args[0]), 16) 
  	}
  
  
        Pids01.list_supported_pids = function(args) {
        	return 0 
  	}
  
        Pids01.monitor_status_since_dtc_cleared = function(args) {
        	return 0 
  	}
  
        Pids01.freeze_dtc = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_system_status = function(args) {
        	return 0 
  	}
  
        Pids01.calculated_engine_load = function(args) {
        	return 0 
  	}
  
        Pids01.short_term_fuel_trim_bank_1 = function(args) {
        	return 0 
  	}
  
        Pids01.long_term_fuel_trim_bank_1 = function(args) {
        	return 0 
  	}
  
        Pids01.short_term_fuel_trim_bank_2 = function(args) {
        	return 0 
  	}
  
        Pids01.long_term_fuel_trim_bank_2 = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.intake_manifold_absolute_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.timing_advance = function(args) {
        	return 0 
  	}
  
        Pids01.intake_air_temperature = function(args) {
        	return 0 
  	}
  
        Pids01.maf_air_flow_rate = function(args) {
        	return 0 
  	}
  
        Pids01.throttle_position = function(args) {
        	return 0 
  	}
  
        Pids01.commanded_secondary_air_status = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_present_in_2_banks = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_short_term_fuel_trim_1 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_short_term_fuel_trim_2 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_short_term_fuel_trim_3 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_short_term_fuel_trim_4 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_short_term_fuel_trim_5 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_short_term_fuel_trim_6 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_short_term_fuel_trim_7 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_short_term_fuel_trim_8 = function(args) {
        	return 0 
  	}
  
        Pids01.vehicle_conforms_to = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_present_in_4_banks = function(args) {
        	return 0 
  	}
  
        Pids01.auxiliary_input_status = function(args) {
        	return 0 
  	}
  
        Pids01.run_time_since_engine_start = function(args) {
        	return 0 
  	}
  
        Pids01.pids_supported_21_40 = function(args) {
        	return 0 
  	}
  
        Pids01.distance_traveled_with_mil = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_rail_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_rail_gaugle_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_voltage_1 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_voltage_2 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_voltage_3 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_voltage_4 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_voltage_5 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_voltage_6 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_voltage_7 = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_voltage_8 = function(args) {
        	return 0 
  	}
  
        Pids01.command_egr = function(args) {
        	return 0 
  	}
  
        Pids01.egr_error = function(args) {
        	return 0 
  	}
  
        Pids01.commanded_evaporative_purge = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_tank_level = function(args) {
        	return 0 
  	}
  
        Pids01.warms_ups_since_codes_cleared = function(args) {
        	return 0 
  	}
  
        Pids01.distance_traveled_since_codes_cleared = function(args) {
        	return 0 
  	}
  
        Pids01.evap_system_vapor_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.absolute_barometric_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_1_current = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_2_current = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_3_current = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_4_current = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_5_current = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_6_current = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_7_current = function(args) {
        	return 0 
  	}
  
        Pids01.oxygen_sensor_fuel_air_8_current = function(args) {
        	return 0 
  	}
  
        Pids01.catalyst_temperature_bank1_sensor1 = function(args) {
        	return 0 
  	}
  
        Pids01.catalyst_temperature_bank2_sensor1 = function(args) {
        	return 0 
  	}
  
        Pids01.catalyst_temperature_bank1_sensor2 = function(args) {
        	return 0 
  	}
  
        Pids01.catalyst_temperature_bank2_sensor2 = function(args) {
        	return 0 
  	}
  
        Pids01.pids_supported_41_60 = function(args) {
        	return 0 
  	}
  
        Pids01.monitor_status_this_drive_cycle = function(args) {
        	return 0 
  	}
  
        Pids01.control_module_voltage = function(args) {
        	return 0 
  	}
  
        Pids01.absolute_load_value = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_air_commanded_equivalence_ratio = function(args) {
        	return 0 
  	}
  
        Pids01.relative_throttle_position = function(args) {
        	return 0 
  	}
  
        Pids01.ambiente_air_temperature = function(args) {
        	return 0 
  	}
  
        Pids01.absolute_throttle_position_b = function(args) {
        	return 0 
  	}
  
        Pids01.absolute_throttle_position_c = function(args) {
        	return 0 
  	}
  
        Pids01.absolute_throttle_position_d = function(args) {
        	return 0 
  	}
  
        Pids01.absolute_throttle_position_e = function(args) {
        	return 0 
  	}
  
        Pids01.absolute_throttle_position_f = function(args) {
        	return 0 
  	}
  
        Pids01.commanded_throttle_actuator = function(args) {
        	return 0 
  	}
  
        Pids01.time_run_with_mil_on = function(args) {
        	return 0 
  	}
  
        Pids01.time_since_trouble_codes_cleared = function(args) {
        	return 0 
  	}
  
        Pids01.maximum_value_for_fuel_air_equivalence_ratio = function(args) {
        	return 0 
  	}
  
        Pids01.maximum_value_for_air_flow_rate_from_mass_air_flow_sensor = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_type = function(args) {
        	return 0 
  	}
  
        Pids01.ethanol_fuel = function(args) {
        	return 0 
  	}
  
        Pids01.absolute_evap_system_vapor_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.evap_system_vapor_pressure_2 = function(args) {
        	return 0 
  	}
  
        Pids01.short_term_secondary_oxygen_sensor_trim_bank_1_3 = function(args) {
        	return 0 
  	}
  
        Pids01.long_term_secondary_oxygen_sensor_trim_bank_1_3 = function(args) {
        	return 0 
  	}
  
        Pids01.short_term_secondary_oxygen_sensor_trim_bank_2_4 = function(args) {
        	return 0 
  	}
  
        Pids01.long_term_secondary_oxygen_sensor_trim_bank_2_4 = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_rail_absolute_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.relative_accelerator_pedal_position = function(args) {
        	return 0 
  	}
  
        Pids01.hybrid_battery_pack_remaining_life = function(args) {
        	return 0 
  	}
  
        Pids01.engine_oil_temperature = function(args) {
        	return int("0x" + "".join(args[0]), 16) - 40 
  	}
  
        Pids01.fuel_injection_timing = function(args) {
        	return 0 
  	}
  
        Pids01.engine_fuel_rate = function(args) {
        	return 0 
  	}
  
        Pids01.emission_requirements_vehicle_is_designed = function(args) {
        	return 0 
  	}
  
        Pids01.pids_supported_61_80 = function(args) {
        	return 0 
  	}
  
        Pids01.drivers_demand_engine_percent_torque = function(args) {
        	return 0 
  	}
  
        Pids01.actual_engine_percent_torque = function(args) {
        	return 0 
  	}
  
        Pids01.engine_reference_torque = function(args) {
        	return 0 
  	}
  
        Pids01.engine_percent_torque_data = function(args) {
        	return 0 
  	}
  
        Pids01.auxiliary_input_output_supported = function(args) {
        	return 0 
  	}
  
        Pids01.mass_air_flow_sensor = function(args) {
        	return 0 
  	}
  
        Pids01.engine_coolant_temperature_2 = function(args) {
        	return 0 
  	}
  
        Pids01.intake_air_temperature_sensor = function(args) {
        	return 0 
  	}
  
        Pids01.commanded_egr_and_egr_error = function(args) {
        	return 0 
  	}
  
        Pids01.commanded_diesel_intake_air_flow_control_and_position = function(args) {
        	return 0 
  	}
  
        Pids01.exhaust_gas_recirculation_temperature = function(args) {
        	return 0 
  	}
  
        Pids01.commanded_throttle_actuator_control_and_position = function(args) {
        	return 0 
  	}
  
        Pids01.fuel_pressure_control_system = function(args) {
        	return 0 
  	}
  
        Pids01.injection_pressure_control_system = function(args) {
        	return 0 
  	}
  
        Pids01.turbocharger_compressor_inlet_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.boost_pressure_control = function(args) {
        	return 0 
  	}
  
        Pids01.variable_geometry_turbo_vgt_control = function(args) {
        	return 0 
  	}
  
        Pids01.wastegate_control = function(args) {
        	return 0 
  	}
  
        Pids01.exhaust_pressure = function(args) {
        	return 0 
  	}
  
        Pids01.turbo_charger_rpm = function(args) {
        	return 0 
  	}
  
        Pids01.turbo_charger_temperature = function(args) {
        	return 0 
  	}
  
        Pids01.turbo_charger_temperature_2 = function(args) {
        	return 0 
  	}
  
        Pids01.charge_air_cooler_temperature = function(args) {
        	return 0 
  	}
  
        Pids01.exhaust_gas_temperature_bank_1 = function(args) {
        	return 0 
  	}
  
        Pids01.exhaust_gas_temperature_bank_2 = function(args) {
        	return 0 
  	}
  
        Pids01.diesel_particulate_filter_1 = function(args) {
        	return 0 
  	}
  
        Pids01.diesel_particulate_filter_2 = function(args) {
        	return 0 
  	}
  
        Pids01.diesel_particulate_filter_3 = function(args) {
        	return 0 
  	}
  
        Pids01.nox_nte_control_area_status = function(args) {
        	return 0 
  	}
  
        Pids01.pm_nte_control_area_status = function(args) {
        	return 0 
  	}
  
        Pids01.engine_run_time = function(args) {
        	return 0 
  	}
  
        Pids01.pids_supported_81_A0 = function(args) {
        	return 0 
  	}
  
        Pids01.engine_run_time_aecd_1 = function(args) {
        	return 0 
  	}
  
        Pids01.engine_run_time_aecd_2 = function(args) {
        	return 0 
  	}
  
        Pids01.nox_sensor = function(args) {
        	return 0 
  	}
  
        Pids01.manifold_surface_temperature = function(args) {
        	return 0 
  	}
  
        Pids01.nox_reagend_system = function(args) {
        	return 0 
  	}
  
        Pids01.pm_sensor = function(args) {
        	return 0 
  	}
  
        Pids01.intake_manifold_absolute_pressure_2 = function(args) {
        	return 0 
  	}
  
        Pids01.pids_supported_A1_C9 = function(args) {
        	return 0 
  	}
  
        Pids01.pids_supported_C1_E0 = function(args) {
        	return 0 
  	}
  
        Pids01.engine_idle_request = function(args) {
        	return 0 
  	}
	
